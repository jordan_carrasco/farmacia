# JAVA Version
1.8

# Para crear JAR y crear imagen docker
mvn clean compile install

# para ejecutar a traves de docker
docker run -p 5000:5000 jordan230891/farmacia-turno:0.0.1-SNAPSHOT

#para ejcutar a traves de spring boot
mvn spring-boot:run

#para ejcutar test
mvn test