package com.consorcio.farmacia2.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.net.URL;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class PharmacyControllerTest {

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void getComunas() throws Exception {

        ResponseEntity<String> response = restTemplate.getForEntity(new URL("http://localhost:" + port + "/comunas").toString(), String.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());

    }

    @Test
    public void getFarmaciaByComuna() throws Exception {

        ResponseEntity<String> response = restTemplate.getForEntity(new URL("http://localhost:" + port + "/farmaciaByComunaLocal?comuna=BUIN").toString(), String.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());

    }
    @Test
    public void getFarmaciaByComunaLocal() throws Exception {

        ResponseEntity<String> response = restTemplate.getForEntity(new URL("http://localhost:" + port + "/farmaciaByComunaLocal?comuna=BUIN&nombreLocal=SALCOBRAND").toString(), String.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());

    }
}
