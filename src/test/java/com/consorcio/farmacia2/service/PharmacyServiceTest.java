package com.consorcio.farmacia2.service;

import com.consorcio.farmacia2.model.County;
import org.junit.jupiter.api.Test;

import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.ApplicationContext;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;


@SpringBootTest
public class PharmacyServiceTest {

    @MockBean
    PharmacyService mockRepository;

    @Autowired
    ApplicationContext context;

    @Test
    void testGetCounties() throws Exception {
        List<County> countyList = new ArrayList<>();
        County county = new County();
        county.setComunaNombre("BUIN");
        countyList.add(county);
        Mockito.when(mockRepository.getCounties()).thenReturn(countyList);

        PharmacyService pharmacyService = context.getBean(PharmacyService.class);
        List<County> countyList1 = pharmacyService.getCounties();
        assertEquals("BUIN", countyList1.get(0).getComunaNombre());

    }
}
