package com.consorcio.farmacia2.controller;

import com.consorcio.farmacia2.model.County;
import com.consorcio.farmacia2.model.Pharmacy;
import com.consorcio.farmacia2.service.PharmacyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by jordanCarrasco on 25/10/19.
 */
@RestController
public class PharmacyController {

    private final PharmacyService service;

    @Autowired
    public PharmacyController(PharmacyService service) {
        this.service = service;
    }

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @RequestMapping(value = "/farmaciaByComunaLocal")
    public List<Pharmacy> getByCountyLocal(@RequestParam("comuna") String county,
                                           @RequestParam(value ="nombreLocal", required = false) String localName) throws Exception {

        List<Pharmacy> pharmacies= service.getPharmacyByCountyLocal(county, localName);
        return pharmacies;
    }

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @RequestMapping(value = "/comunas")
    public ResponseEntity<List<County>> getCounties() throws Exception {

        List<County> countyList = service.getCounties();
        return new ResponseEntity<>(
                countyList,
                HttpStatus.OK);
    }
}
