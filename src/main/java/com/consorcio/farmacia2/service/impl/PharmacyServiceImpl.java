package com.consorcio.farmacia2.service.impl;

import com.consorcio.farmacia2.model.County;
import com.consorcio.farmacia2.model.Pharmacy;
import com.consorcio.farmacia2.service.PharmacyService;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.reflect.TypeToken;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


@Service
public class PharmacyServiceImpl implements PharmacyService {

    @Value("${operations.restURL}")
    String serviceURL;

    @Value("${operations.restURL2}")
    String serviceURL2;


    @Override
    public List<Pharmacy> getPharmacyByCountyLocal(String county, String local) throws Exception {
        RestTemplate restTemplate = new RestTemplate();
        String farmacias = restTemplate.getForObject(serviceURL, String.class);
        JsonArray jsonObject = new Gson().fromJson(farmacias, JsonArray.class);
        List<Pharmacy> yourArray = new Gson().fromJson(jsonObject.toString(), new TypeToken<List<Pharmacy>>(){}.getType());
        List<Pharmacy> result = yourArray.stream().filter(line -> county.equalsIgnoreCase(line.getComuna_nombre())).collect(Collectors.toList());
        if (local != null) {
            return result.stream().filter(line -> local.equalsIgnoreCase(line.getLocal_nombre())).collect(Collectors.toList());
        }
        return result;
    }

    @Override
    public List<County> getCounties() throws Exception {
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

        MultiValueMap<String, String> map= new LinkedMultiValueMap<String, String>();
        map.add("reg_id", "7");

        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(map, headers);

        ResponseEntity<String> response = restTemplate.postForEntity( serviceURL2, request , String.class);

        Document doc = Jsoup.parse(response.getBody());
        List<County> countyList = new ArrayList();
        int cont = 0;
        for (Element option : doc.select("option")) {
            if (cont > 0) {
                County county = new County();
                county.setComunaNombre(option.text());
                countyList.add(county);
            }
            cont++;
        }
        return countyList;
    }

}
