package com.consorcio.farmacia2.service;

import com.consorcio.farmacia2.model.County;
import com.consorcio.farmacia2.model.Pharmacy;

import java.util.List;

/**
 * Created by jordanCarrasco on 25/10/19.
 */
public interface PharmacyService {

    List<Pharmacy> getPharmacyByCountyLocal(String county, String local) throws Exception;
    List<County> getCounties() throws Exception;
}
